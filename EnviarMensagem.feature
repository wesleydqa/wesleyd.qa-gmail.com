# language: pt
Funcionalidade: Enviar mensagem
  Com um usuário do WhatsApp
  Para testar o envio de mensagens
  Os usuário do WhatsApp devem conseguir enviar mensagens para outros usuários

  Cenário: Enviar mensagem de texto
    Dado estes usuários:
    | username      |
    | 5511976546756 |
    | 5511987653456 |
    E aplicativo WhatsApp aberto
    E um usuário selecionado para o envio da mensagem
    E uma mensagem aberta para envio
    Quando digitar a <mensagem>
    E clicar no botão ">"
    Então o usuário selecionado deve receber a mensagem enviada
    E a mensagem deve conter todos os caracteres enviados
    E a mensagem deve ter a mesma formação da mensagem enviada

    Exemplos:
      | mensagem                      |
      | Ola                            |
      | Teste!@#$%¨&*()""''^~´`{}[]<>; |
      | Mensagem com mais de 500 caracteres Mensagem com mais de 500 caracteres Mensagem com mais de 500 caracteress Mensagem com mais de 500 caracteres Mensagem com mais de 500 caracteres Mensagem com mais de 500 caracteress Mensagem com mais de 500 caracteres Mensagem com mais de 500 caracteres Mensagem com mais de 500 caracteress Mensagem com mais de 500 caracteres Mensagem com mais de 500 caracteres Mensagem com mais de 500 caracteress Mensagem com mais de 500 caracteres Mensagem com mais de 500                        |
      |                                |

  Cenário: Enviar mensagem de voz
    Dado estes usuários:
    | username | password | email               |
    | everzet  | 123456   | everzet@knplabs.com |
    | fabpot   | 22@222   | fabpot@symfony.com  |
    E aplicativo WhatsApp aberto
    E um usuário selecionado para o envio da mensagem
    E uma mensagem aberta para envio
    Quando pressionar no botão com o simbolo de microfone
    E Mantelo pressionado
    E falar a mensagem de voz
    E Soltar o botão
    Então o usuário selecionado deve receber a mensagem de voz enviada
    E a mensagem de voz deve conter o mesmo audio gravado
    E a mensagem de voz não deve conter ruidos diferentes do ambiente