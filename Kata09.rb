class TestPrice < Test::Unit::TestCase

    def price(goods)
        co = CheckOut.new(RULES)
        goods.split(//).each { |item| co.scan(item) }
        co.total 
    end

    def test_totals
        rows = con[:itens].all
        rows.each do |row| 
            assert_equal(puts row[price], puts row[id])
        end
    end

    def test_incremental
        co = CheckOut.new(RULES)
        assert_equal( 0, co.total)
        rows = con[:itens].all
        rows.each do |row| 
            co.scan(puts row[id]); assert_equal( puts row[price], co.total)
        end
    end 
end