# language: pt
Funcionalidade: Sair do grupo
  Com um usuário do WhatsApp
  Para testar a exclusão de uma conta de um grupo
  O WhatsApp deve permitir a exclusão da conta

  Cenário: Enviar mensagem de texto
    Dado estes usuários:
    | username      |
    | 5511976546756 |
    | 5511987653456 |
    E aplicativo WhatsApp aberto
    E um grupo selecionado para solicitar à exclusão
    Quando Clicar em "..."
    E clicar em "Sair do grupo"
    E Clicar em "Sair" na mensagem exibida
    Então deve ser exibida a mensagem "Você saiu" no grupo selecionado
    E não deve permitir o envio de nova mensagem ao grupo